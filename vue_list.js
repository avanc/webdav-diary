export default {
  props: {
    items: Array
  },
  template: `
  <li v-for="item in items">
    {{ item }}
  </li>
  `
}
