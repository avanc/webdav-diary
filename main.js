import { createApp } from 'vue'
import { createVuetify } from 'vuetify'
const vuetify = createVuetify()

import vueApp from 'vue-app'

var app=createApp(vueApp).use(vuetify).mount('#app')


