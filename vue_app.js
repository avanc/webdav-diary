import { ref, watch, onMounted, toRaw } from 'vue'
import vueList from 'vue-list'
import vueInput from 'vue-input'
import vueConfig from 'vue-config'


import { createClient } from "webdav";
var client=false

async function getHistory() {
  const directoryItems = await client.getDirectoryContents("/")

  const re = /^diary_(\d\d\d\d-\d\d\-\d\d).txt$/;
  var diary_history=[];

  for (const item of directoryItems) {
    if (item.type == "file") {
      var re_result = item.basename.match(re)
      if (re_result) {
        diary_history.push(re_result[1])
      }
    }
  }

  return diary_history.sort(compareDates)
}


function compareDates(a, b) {
  if (a > b)
    return -1;
  if (a < b)
    return 1;
  return 0;
}


async function writeDiaryItem(date, content) {
  var filename = "diary_" + date + ".txt"
  var result = await client.putFileContents(filename, content, { overwrite: false });

  return result
}



export default {
  components: {
    vueList,
    vueInput,
    vueConfig
  },
  setup() {
    const history = ref([])
    const webdavconf = ref({})


    async function submitEntry(msg) {
      if (await writeDiaryItem(msg.date, msg.text)) {
        history.value = await getHistory()
      }
    }

    function getConfig(config) {
      console.log(config)
      webdavconf.value=structuredClone(toRaw(config))
      writeConfig()
    }

    function readConfig() {
      console.log("Reading Config")
      let newObject = window.localStorage.getItem("webdavConf");
      webdavconf.value=JSON.parse(newObject);
      console.log(newObject)
    }



    function writeConfig() {
      var config = structuredClone(toRaw(webdavconf.value));
      if (!config["store_password"]) {
        delete config.password
      }
      window.localStorage.setItem("webdavConf", JSON.stringify(config));
    }


    async function initWebdavClient() {
      console.log("Connecting")
      client = createClient(
        webdavconf.value.url,
        {
          username: webdavconf.value.username,
          password: webdavconf.value.password
        }
      )
      history.value = await getHistory()
    }

    watch(webdavconf, (webdavconf) => {
      console.log("webdavconf changed")
      initWebdavClient()
    })


    onMounted(() => {
      readConfig()
      //initWebdavClient()
    })



    return {
      history,
      submitEntry,
      webdavconf,
      getConfig
    }
  },
  template: `
    <v-app>
      <v-main>
        <vue-input @submit="submitEntry"></vue-input>
        <vue-list :items="history"></vue-list>
        <vue-config :webdavConfInit="webdavconf" @connect="getConfig"></vue-config>
      </v-main>
    </v-app>
  `
}

