import { ref, computed } from 'vue'

function formatDate(date) {
  if (!(date instanceof Date)) {
    throw new Error('Invalid "date" argument. You must pass a date instance')
  }

  const year = date.getFullYear()
  const month = String(date.getMonth() + 1).padStart(2, '0')
  const day = String(date.getDate()).padStart(2, '0')

  return `${year}-${month}-${day}`
}

export default {
  emits: ['submit'],
  setup(props, { emit }) {
    const limit = ref(140)
    const text = ref('')
    const date = ref(formatDate(new Date()))
    const remaining = computed(() => {
      return limit.value - text.value.length
    })

    function submit() {
      emit('submit', {"date": date.value, "text": text.value})
    }

    return {
      text,
      date,
      remaining,
      submit
    }
  },
  template: `
    <input v-model="date" name="date" type="date">
    <textarea v-model="text" name="entry" placeholder="Dear Diary..." required autofocus rows="3" cols="50" maxlength="140"></textarea>
    <span id="counter">{{ remaining }}</span>
    <button type="button" @click="submit">Submit</button>
  `
};
