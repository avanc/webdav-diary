import { ref, computed, watch, defineProps, toRefs, onMounted} from 'vue'

export default {
  emits: ['connect'],
  props: {
    webdavConfInit: Object
  },
  setup(props, { emit }) {
    const url = ref("https://")
    const username = ref('')
    const password = ref('')
    const store_password = ref(false)

    const { webdavConfInit } = toRefs(props)

    const remaining = computed(() => {
      return limit.value - text.value.length
    })

    function connect() {
      emit('connect', {
        "url": url.value,
        "username": username.value,
        "password": password.value,
        "store_password": store_password.value
      })
    }

    watch(webdavConfInit, (webdavConfInit) => {
      if (props.webdavConfInit.url)
        url.value=props.webdavConfInit.url
      if (props.webdavConfInit.username)
        username.value=props.webdavConfInit.username
      if (props.webdavConfInit.password)
        password.value=props.webdavConfInit.password
      if (props.webdavConfInit.store_password)
        store_password.value=props.webdavConfInit.store_password
      console.log(`x is ${webdavConfInit}`)
    })

    onMounted(() => {
      if (props.webdavConfInit.url)
        url.value=props.webdavConfInit.url
      if (props.webdavConfInit.username)
        username.value=props.webdavConfInit.username
      if (props.webdavConfInit.password)
        password.value=props.webdavConfInit.password
      if (props.webdavConfInit.store_password)
        store_password.value=props.webdavConfInit.store_password
    })

    return {
      url,
      username,
      password,
      store_password,
      connect
    }
  },

  template: `
<v-card
    class="mx-auto"
    max-width="344"
    title="Webdav Configuration"
  >
    <v-container>
      <v-text-field
        v-model="url"
        color="primary"
        label="Webdav URL"
        variant="underlined"
      ></v-text-field>

      <v-text-field
        v-model="username"
        color="primary"
        label="User Name"
        variant="underlined"
      ></v-text-field>

      <v-text-field
        v-model="password"
        color="primary"
        label="Password"
        placeholder="Enter your password"
        variant="underlined"
        type="password"
      ></v-text-field>

      <v-checkbox
        v-model="store_password"
        color="secondary"
        label="Store password in browser cache"
      ></v-checkbox>
    </v-container>

    <v-divider></v-divider>

    <v-card-actions>

      <v-btn color="success" @click="connect">
        Connect
      </v-btn>
    </v-card-actions>
  </v-card>
  `
};
